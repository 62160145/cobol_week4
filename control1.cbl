       IDENTIFICATION DIVISION. 
       PROGRAM-ID. APPBMI.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT   PIC   999V99 VALUE ZEROS .
       01  HEIGHT   PIC   999V99 VALUE   ZEROS.
       01  BMI PIC 99V99 VALUE ZEROS.
       01  BMI-DETAIL  PIC   X(35) VALUE ZEROS.
           88 UNDER-WEIGHT   VALUE "Your bmi is under weight. ".
           88 NORMAL-WEIGHT   VALUE "Your bmi is normal. ".
           88 OVER-WEIGHT   VALUE "Your bmi is over weight. ".
           88 OBESE   VALUE "Your bmi is OBESE. ".
           88 EXTREMLY-OBESE   VALUE "Your bmi is extremly obese. ".

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Enter your weight(KG) :" WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Enter your height(CM) :" WITH NO ADVANCING 
           ACCEPT HEIGHT  
      *    เปลี่ยนหน่วย CM to M
           COMPUTE HEIGHT = HEIGHT / 100
           END-COMPUTE

           
      *    คำนวณหา BMI
           COMPUTE BMI=WEIGHT / (HEIGHT **2)
           END-COMPUTE 

           EVALUATE TRUE  
              WHEN  BMI<18.50 SET UNDER-WEIGHT TO  TRUE
              WHEN  BMI>=18.50 AND BMI<22.90 SET NORMAL-WEIGHT TO  TRUE
              WHEN  BMI>=23.00 AND BMI<=24.90 SET OVER-WEIGHT TO  TRUE
              WHEN  BMI>=25.00 AND BMI<=29.90 SET OBESE TO  TRUE
              WHEN  BMI>=30.00 SET EXTREMLY-OBESE TO  TRUE
           END-EVALUATE
           

           DISPLAY "BMI: "BMI 
           DISPLAY "DETAIL: "BMI-DETAIL 
           .
